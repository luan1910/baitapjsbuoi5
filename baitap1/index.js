function KhuVuc(dkv) {
  var diemKhuVuc = 0;

  if (dkv == "A") {
    diemKhuVuc = 2;
  }
  if (dkv == "B") {
    diemKhuVuc = 1;
  }
  if (dkv == "C") {
    diemKhuVuc = 0.5;
  }
  if (dkv == "X") {
    diemKhuVuc = 0;
  }
  return diemKhuVuc;
}

function DoiTuong(ddt) {
  var diemDoiTuong = 0;

  if (ddt == "khong") {
    diemDoiTuong = 0;
  }
  if (ddt == "mot") {
    diemDoiTuong = 2.5;
  }
  if (ddt == "hai") {
    diemDoiTuong = 1.5;
  }
  if (ddt == "ba") {
    diemDoiTuong = 1;
  }
  return diemDoiTuong;
}

function ketQua() {
  var kv = document.getElementById("txt-khu-vuc");
  var khuVuc = kv.options[kv.selectedIndex].value;
  console.log("khuVuc: ", khuVuc);

  var dt = document.getElementById("txt-doi-tuong");
  var doiTuong = dt.options[dt.selectedIndex].value;
  console.log("doiTuong: ", doiTuong);

  var tinhDiemKhuVuc = KhuVuc(khuVuc);
  var tinhDiemDoiTuong = DoiTuong(doiTuong);

  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemMon1 = document.getElementById("txt-diem-1").value * 1;
  var diemMon2 = document.getElementById("txt-diem-2").value * 1;
  var diemMon3 = document.getElementById("txt-diem-3").value * 1;

  var tongDiem =
    diemMon1 + diemMon2 + diemMon3 + tinhDiemKhuVuc + tinhDiemDoiTuong;
  var result = "";

  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    result = `Bạn Đã Rớt. Tổng Điểm: ${tongDiem}`;
  } else if (tongDiem >= diemChuan) {
    result = `Bạn Đã Đậu. Tổng Điểm: ${tongDiem}`;
  } else {
    result = `Bạn Đã Rớt. Tổng Điểm: ${tongDiem}`;
  }

  document.getElementById("result").innerText = result;
}
