function tinhDien() {
  // console.log("Yess");

  var soDien_50kWdau = 500;
  var soDien_50kWke = 650;
  var soDien_100kWke = 850;
  var soDien_150kWke = 1100;
  var soDien_kWconlai = 1300;

  var sokW = document.getElementById("txt-so-dien").value * 1;

  var result = "";

  if (sokW <= 50) {
    result = sokW * soDien_50kWdau;
  } else if (sokW <= 100) {
    result = soDien_50kWdau * 50 + (sokW - 50) * soDien_50kWke;
  } else if (sokW <= 200) {
    result =
      soDien_50kWdau * 50 + soDien_50kWke * 50 + (sokW - 100) * soDien_100kWke;
  } else if (sokW <= 350) {
    result =
      soDien_50kWdau * 50 +
      soDien_50kWke * 50 +
      soDien_100kWke * 100 +
      (sokW - 200) * soDien_150kWke;
  } else {
    result =
      soDien_50kWdau * 50 +
      soDien_50kWke * 50 +
      soDien_100kWke * 100 +
      soDien_150kWke * 150 +
      (sokW - 350) * soDien_kWconlai;
  }
  const formatter = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
    minimumFractionDigits: 0,
  });
  var newResult = formatter.format(result);
  document.getElementById("result").innerText = newResult;
}
